import { Injectable } from '@angular/core';  // First se importa el objeto Injectable
import { Http, RequestOptions, Headers } from '@angular/http'; // De tercero se importa Http


import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Project } from '../models/project-model';
import {AuthenticationService} from '../../../common/services/authentication.service';
import {HttpService} from '../../../common/services/http.service';




@Injectable() // De segundo se agrega este objeto
// De cuarto se genera Instancia de Http
export class ProjectIstService extends HttpService{
    constructor(public _http: Http, public _authService: AuthenticationService) {

        super(_http);
    }
    getAll(): Observable<Array<Project>> {
        const url = `${this.apiBaseURL}/projects`;
        const token = this._authService.user.api_token; // Se declara la variable y se le pasa el token que viene el Auth

        return this.get(url, token);
    }

    deleteProject(project: Project) {
        const url = `${this.apiBaseURL}/projects/${project.id}`; // Le paso el id del proyecto que voy a eliminar
        const token = this._authService.user.api_token;

        return this.delete(url, token); // Se crea el método para eliminar proyecto
    }
}

// Nuestro ProjectListService heredatodo lo que tenga HttpService